
public class Arithmetic {
    	// main method required for every Java program
   	public static void main(String[] args) {
      //Number of pairs of pants
      int numPants = 3;
      //Cost per pair of pants
      double pantsPrice = 34.98;

      //Number of sweatshirts
      int numShirts = 2;
      //Cost per shirt
      double shirtPrice = 24.99;

      //Number of belts
      int numBelts = 1;
      //cost per belt
      double beltCost = 33.99;

      //the tax rate
      double paSalesTax = 0.06;
      
      double totalCostOfPants;  //total cost of pants
      double totalCostOfSweatshirts;  //total cost of sweatshirts
      double totalCostOfBelts;  //total cost of belts
      double totalSalesTax; //total sales tax
      
      //total cost of each kind of item
      totalCostOfPants = numPants * pantsPrice;
      totalCostOfSweatshirts = numShirts * shirtPrice;
      totalCostOfBelts = numBelts * beltCost;
      
      System.out.println("The total cost for the pants purchased is "+(totalCostOfPants)+" dollars.");
      System.out.println("The total cost for the sweatshirts purchased is "+(totalCostOfSweatshirts)+" dollars.");
      System.out.println("The total cost for the belts purchased is "+(totalCostOfBelts)+" dollars.");
      
      //Sales tax charged buying all of each kind of item
      System.out.println("The total sales tax collected for the pants purchased is "+(((int)(totalCostOfPants*paSalesTax*100))/100.00)+" dollars.");
      System.out.println("The total sales tax collected for the sweatshirts purchased is "+(((int)(totalCostOfSweatshirts*paSalesTax*100))/100.00)+" dollars.");
      System.out.println("The total sales tax collected for the belts purchased is "+(((int)(totalCostOfBelts*paSalesTax*100))/100.00)+" dollars.");

      //Total cost of purchases (before tax)
      System.out.println("The total cost of purchases is "+(totalCostOfPants+totalCostOfSweatshirts+totalCostOfBelts)+" dollars.");
      
      //Total sales tax
      totalSalesTax=(totalCostOfPants+totalCostOfSweatshirts+totalCostOfBelts)*paSalesTax;
      System.out.println("The total sales tax collected for this purchase is "+(((int)(totalSalesTax*100))/100.00)+" dollars.");
      
      //Total paid for this transaction, including sales tax.
      System.out.println("The total paid for this purchase, including sales tax, is "+(((int)(((totalCostOfPants+totalCostOfSweatshirts+totalCostOfBelts)+totalSalesTax)*100))/100.00)+" dollars.");
    }
}