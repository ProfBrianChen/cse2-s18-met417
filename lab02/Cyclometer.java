//Maeve Tules
//2.8.18
//CSE2
//Gain information from a cyclometer
public class Cyclometer {
    	// main method required for every Java program
   	public static void main(String[] args) {
    // our input data. Document your variables by placing your
		// comments after the State the purpose of each variable.
      int secsTrip1=480;  //Time of trip 1, seconds
      int secsTrip2=3220;  //Time of trip 2, seconds
		  int countsTrip1=1561;  //Front wheel rotations of trip 1
		  int countsTrip2=9037; //Front wheel rotations of trip 2
      double wheelDiameter=27.0,  //Front wheel diameter
      
  	  PI=3.14159, //Numerical value of pi, for calculating distance
  	  feetPerMile=5280,  //Feet per miles, for calculating distance
  	  inchesPerFoot=12,   //Inches per foot, for calculating distance
  	  secondsPerMinute=60;  //Seconds per minutes, for converting recorded seconds to something more readable
	    double distanceTrip1, distanceTrip2,totalDistance;  //Declaring variables
      
      System.out.println("Trip 1 took "+
       	     (secsTrip1/secondsPerMinute)+" minutes and had "+
       	      countsTrip1+" counts.");
	       System.out.println("Trip 2 took "+
       	     (secsTrip2/secondsPerMinute)+" minutes and had "+
       	      countsTrip2+" counts.");

		  //run the calculations; store the values. Document your
		  //calculation here. What are you calculating?
		  //Converting the recoded seconds of each trip and converting to minutes
	    distanceTrip1=countsTrip1*wheelDiameter*PI;
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
	    distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
	    distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
	    totalDistance=distanceTrip1+distanceTrip2;
      
      //Print out the output data.
      System.out.println("Trip 1 was "+distanceTrip1+" miles");
	    System.out.println("Trip 2 was "+distanceTrip2+" miles");
	    System.out.println("The total distance was "+totalDistance+" miles");




	}  //end of main method   
} //end of class
