import java.util.Random; //makes available the Random class

public class CardGenerator{
    			// main method required for every Java program
   			public static void main(String[] args) {
          int rand = (int)(Math.random()*51)+1; //creates a random int called rand
          String suit; //creates variable suit of the string type
          String identityString; //creates variable identityString of the string types
          //assigns suit to the randomly created number rand
          if ( rand <= 13 ) {
            suit = "diamonds";
          }
          else if ( rand <= 26 ) {
            suit = "clubs";
          }
          else if ( rand <= 39 ) {
            suit = "hearts";
          }
          else {
            suit = "spades";
          } 
          //assings an idenity to the randomly created number rand
          switch ( rand % 13 ) {
            case 0:
              identityString = "1";
              break;
            case 1:
              identityString = "2";
              break;
            case 2:
              identityString = "3";
              break;
            case 3:
              identityString = "4";
              break;
            case 4:
              identityString = "5";
              break;
            case 5:
              identityString = "6";
              break;
            case 6:
              identityString = "7";
              break;
            case 7:
              identityString = "8";
              break;
            case 8:
              identityString = "9";
              break;
            case 9:
              identityString = "10";
              break;
            case 10:
              identityString = "Jack";
              break;
            case 11:
              identityString = "Queen";
              break;
            case 12:
              identityString = "King";
              break;
            case 13:
              identityString = "Ace";
              break;
            default:
              identityString = "Broken";
              break;
            }
          System.out.println("Your card is " + identityString +  " of " + suit ); //prints the randombly created card suit and identity
          
         
          
        }
         
}