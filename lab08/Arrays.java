import java.util.Random;
import java.util.Scanner;

public class Arrays {
  public static void main(String[] args){
    Scanner myScanner = new Scanner( System.in );
    Random rand = new Random();
    //create array of Strings named "students" of a randomized size from 5 to 10
    final int NUM_STUDENTS = rand.nextInt(10) + 5;
    String[] students;
    students = new String[NUM_STUDENTS];
    //ask the user to fill up the array with different Strings using a scanner
    int i = 0;
    for(i = 0; i < NUM_STUDENTS; i++) {
      System.out.println("input student names");
      myScanner.hasNextLine();
      students[i] = myScanner.nextLine();
    }
    //create a second array named "midterm" of the same size as "students"
    int[] midterm;
    midterm = new int[NUM_STUDENTS];
    int j = 0;
    for(j = 0; j < NUM_STUDENTS; j++) {
      System.out.println("input student grades (0-100)");
      myScanner.hasNextLine();
      midterm[j] = myScanner.nextInt();
    }
    System.out.println("Here are the midterm grades of the 5 students above ");
    //print out the members of the two arrays such that each line has a member of "students" and "midterm"
    for(i = 0; i < NUM_STUDENTS; i++) {
      System.out.println( students[i] + ": " + midterm[i]);
    }
  }
}