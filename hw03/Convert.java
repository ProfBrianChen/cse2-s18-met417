//Maeve Tules
//2.8.18
//CSE2

//Acres of land affected by precipitation
//Average rain fall, inches
//Calculate quantity of rain, cubic miles
import java.util.Scanner; //Imports the scanner class, which will be used later in the program
public class Convert{
    			// main method required for every Java program
   			public static void main(String[] args) {
          
          Scanner myScanner = new Scanner( System.in );//Creates the scanner myScanner, which calls the user to input a variable
          
          System.out.print("Enter the affected area in acres: ");
          double totalAcres = myScanner.nextDouble();//assigns the users input variable, the total acres, as a double named totalAcres
                           
          System.out.print("Enter the inches of rainfall in the affected area:  ");
          double rainInches = myScanner.nextDouble();//assigns the users input variable, inches of rainfall, as a double named rainInches
          
          
          //Convert acres into square miles
          double squareMiles;
          squareMiles = totalAcres*0.0015625;
          
          //Convert inches into miles
          double milesRain;
          milesRain = rainInches*0.000015783;
          
          double cubicMiles;
          cubicMiles = squareMiles * milesRain;
         
          System.out.println(cubicMiles + "cubic miles");

          }  //end of main method   
  	} //end of class


