//Maeve Tules
//2.8.18
//CSE2

import java.util.Scanner; //Imports the scanner class, which will be used later in the program
public class Pyramid{
    			// main method required for every Java program
   			public static void main(String[] args) {
          
          Scanner myScanner = new Scanner( System.in );//Creates the scanner myScanner, which calls the user to input a variable
          
          System.out.print("The square side of the pyramid is (input length): ");
          double sideLength = myScanner.nextDouble();//assigns the users input variable, the total acres, as a double named totalAcres
                           
          System.out.print("The height of the pyramid is (input height):  ");
          double pyramidHeight = myScanner.nextDouble();//assigns the users input variable, inches of rainfall, as a double named rainInches
          
          
          //Convert inches into miles
          double pyramidVolume;
          pyramidVolume = (sideLength*sideLength*pyramidHeight)/3;
         
          System.out.println("The volume inside the pyramid is:"+pyramidVolume);

          }  //end of main method   
  	} //end of class
